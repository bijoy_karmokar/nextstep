<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Validator;

class MailController extends Controller
{
    public function index(Request $request) {
        $validator = Validator::make($request->input(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email'],
            'message' => ['required', 'string', 'max:255',]
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with('error',['type' => 'danger','msg' =>'Something wrong !!']);
        } else {
			$data = array('name' => $request->name,
						  'message' => $request->message,
						  'email' => $request->email,
			);

			// \Mail::send('email_template', $data, function($message){
			// 	$message->to('arnobsec21@gmail.com','test mail')
			// 	->subject('TEST EMAIL');
			// 	$message->from('arnobsec21@gmail.com','test mail');
			// });
			if (Mail::to('bijoykarmokar71@gmail.com')->send(new SendMail($data))){
				return back()->with('success', 'We are recived your message. Thanks for contact with us');
			}
			//dd($data);
        }
    }
}
