<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', function () { return view('about'); })->name('about');
Route::get('/service', function () { return view('service'); })->name('service');
Route::get('/career', function () { return view('career'); })->name('career');
Route::get('/contact', function () { return view('contact'); })->name('contact');
Route::get('/friendly', function () { return view('friendly'); })->name('friendly');
Route::get('/flappycash', function () { return view('flappycash'); })->name('flappycash');
Route::get('/ludo', function () { return view('ludo'); })->name('ludo');

/*------------- Service section -------------*/
Route::get('/vas', function () { return view('vas'); })->name('vas');
Route::get('/digital_markating', function () { return view('digital_markating'); })->name('digital_markating');
Route::get('/ecommerce', function () { return view('ecommerce'); })->name('ecommerce');
Route::get('/import_export', function () { return view('import_export'); })->name('import_export');
Route::get('/travel_service', function () { return view('travel_service'); })->name('travel_service');
Route::get('/student_consultancy', function () { return view('student_consultancy'); })->name('student_consultancy');

/*------------- Marketing section -------------*/
Route::get('/email_marketing', function () { return view('email_marketing'); })->name('email_marketing');
Route::get('/sms_marketing', function () { return view('sms_marketing'); })->name('sms_marketing');
Route::get('/facebook_marketing', function () { return view('facebook_marketing'); })->name('facebook_marketing');
Route::get('/youtube_marketing', function () { return view('youtube_marketing'); })->name('youtube_marketing');
Route::get('/faq', function () { return view('faq'); })->name('faq');
Route::post('/mail/send', 'MailController@index')->name('send_mail');