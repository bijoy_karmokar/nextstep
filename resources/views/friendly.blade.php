@extends('layouts.single')

@section('content')
<div class="breadcrumb-w3pvt">
	<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
                <a href="{{URL::to('/')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('service')}}">Product</a>
            </li>
            <li class="breadcrumb-item" aria-current="page">Friendly</li>
		</ol>
	</nav>
	</div>
</div>
<div class="outerdiv">
<div class="container">
	<div class="row">
		<div class="nt_app_body">
			<div class="col-md-4">
				<img src="{{asset('images/app/friendly/friendly_app_icon.png')}}">
			</div>
			<div class="">
				<h3>Friend.ly</h3>
				<p>CyberBlaze Technologies</p>
			</div>
		</div>
	</div>
	<div class="row btndw">
          <div class="col mb-2">
            <!-- <button type="button" class="btn btn-success btn-block" onclick="window.open('FlappyCash.apk')"><i class="fas fa-download pr-2"></i>ডাউনলোড</button> -->
            <a href="http://cyberappsbd.com/friendly/download" class="btn btn-success btn-block"><i class="fa fa-download pr-2"></i>Download</a>
          </div>
        </div>
	<div class="row mt-4 mb-4">
          <div class="col">
            <div class="row">
              <div class="col-4 text-center info">
                <span>
                  4.9 <i class="fa fa-star"></i>
                </span><br>
                <span class="info_details">34K reviews </span>
              </div>
              <div class="col-4 text-center info">
                 <span>
                  <i class="fa fa-download"></i>
                </span><br>
                <span class="info_details">5 MB </span>
              </div>
              <div class="col-4 text-center infolast">
                <span>
                 1M+ <i class="fa fa-users"></i>
                </span><br>
                <span class="info_details">Downloads </span>
              </div>
              
            </div>
          </div>

        </div>
	<div class="row">
          <div class="col pl-4 pr-4">
            <div class="owl-carousel">
              <div class="pl-2 pr-2"><img src="{{asset('images/app/friendly/1.jpg')}}" class="img-fluid"></div>
              <div class="pl-2 pr-2"><img src="{{asset('images/app/friendly/2.jpg')}}" class="img-fluid"></div>
              <div class="pl-2 pr-2"><img src="{{asset('images/app/friendly/3.jpg')}}" class="img-fluid"></div>
              <div class="pl-2 pr-2"><img src="{{asset('images/app/friendly/4.jpg')}}" class="img-fluid"></div>
              <div class="pl-2 pr-2"><img src="{{asset('images/app/friendly/5.jpg')}}" class="img-fluid"></div>
              <!-- <div class="pl-2 pr-2"><a href="slider/s6.jpg"><img src="../slider/s6.jpg" class="img-fluid"></a></div>  -->
              <div class="pl-2 pr-2"><img src="{{asset('images/app/friendly/6.jpg')}}" class="img-fluid"></div>
            </div>  
          </div>
    </div>
</div>
</div>
    <script src="http://cyberappsbd.com/slider/owl.carousel.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
      $(".owl-carousel").owlCarousel();
      // $('.collapse').collapse();
    });

  $('.owl-carousel').owlCarousel({
          loop: true,
          items: 3, 
          autoplay:true,
          dots: false,
          nav: true,
          navText: ["<i class='fa fa-long-arrow-left'></i>","<i class='fa fa-long-arrow-right'></i>"],

      });

    $( ".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');
    </script>
@endsection