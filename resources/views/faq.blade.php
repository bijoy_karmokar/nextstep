@extends('layouts.single')

@section('content')
<div class="breadcrumb-w3pvt">
	<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
                <a href="{{URL::to('/')}}">Home</a>
            </li>
            <li class="breadcrumb-item" aria-current="page">Frequently Asked Questions</li>
		</ol>
	</nav>
	</div>
</div>


<div class="container py-md-5 py-sm-3">
	<!-- <div class="row">
		<div class="col-12">
				<div class="tab_in_sec">
					<h1 class="heading mb-5">
					Have any Question?
				</h1>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				  <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
					  <h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="" onclick="ga('send', 'event', 'Button', 'Click', 'Program Overview', 1);">
						   Q.&nbsp; When NextStep eastablished?
						</a>
					  </h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
					  <div class="panel-body">
						<p><span style="font-weight: bold;">Ans:</span>&nbsp; Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>								
						
					</div>
					</div>
				  </div>
				  <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingTwo">
					  <h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" onclick="ga('send', 'event', 'Button', 'Click', 'Program Eligibility', 1);">
							Q.&nbsp; When NextStep eastablished?
						</a>
					  </h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
							<p> <span style="font-weight: bold;">Ans:</span>&nbsp;
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Ut sed eros tempor, varius ipsum vel, viverra nisl.
                            Nam elementum lacus vitae dui ultrices, quis scelerisque ex gravida.
                            Nulla feugiat nulla et ligula luctus, non placerat urna pulvinar.
                            Nullam vitae neque vitae sem volutpat scelerisque mattis nec tortor.
                            Nullam condimentum est eu magna sagittis, a pretium ligula rhoncus.
                            Curabitur sollicitudin erat at dui vulputate, dignissim pharetra tellus facilisis.
                            Nam ornare ipsum id lectus cursus, eget molestie sem tempus.
                            Quisque faucibus elit sed sapien viverra, sit amet pellentesque est egestas.
                            Praesent sit amet lacus a libero commodo blandit sit amet a diam.
                            Quisque vitae metus non leo aliquam convallis eget facilisis nulla.
                            Quisque sit amet urna ullamcorper, volutpat turpis quis, ornare diam.
                            Ut vitae felis ac mi viverra sagittis.
                            Nam euismod erat vel ipsum maximus, sed gravida eros posuere.</p>
						</div>
					</div>
				  </div>
				  <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingThree">
					  <h4 class="panel-title">
						<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree" onclick="ga('send', 'event', 'Button', 'Click', 'Why NMIMS', 1);">
						 Q.&nbsp; When NextStep eastablished?
						</a>
					  </h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
					  <div class="panel-body">
                        <p><span style="font-weight: bold;">Ans:</span> &nbsp; Sed ultricies nulla id nulla rhoncus efficitur.
                        Sed eu mi et tortor imperdiet pellentesque.
                        Aenean eu mi vitae urna tempor imperdiet rutrum ac tortor.
                        Maecenas pharetra erat non est condimentum tempor.</p>
					  </div>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</div> -->


	<!-- another way -->

	<div class="row" style="margin-top: 20px">
		<div class="col-md-6">
                                <div class="pxlr-club-faq">
                                    <div class="content">
                                        <div class="panel-group" id="accordion" role="tablist"
                                             aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" id="headingOne" role="tab">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse"
                                                           data-parent="#accordion" href="#collapseOne"
                                                           aria-expanded="false" aria-controls="collapseOne"><b>Q.</b>
                                                            Anonymous user won’t receive
                                                            email if question is private? <i
                                                                    class="pull-right fa fa-plus"></i></a>
                                                    </h4>
                                                </div>
                                                <div class="panel-collapse collapse" id="collapseOne" role="tabpanel"
                                                     aria-labelledby="headingOne">
                                                    <div class="panel-body pxlr-faq-body">
                                                        <p><b>Answer: </b>Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                            accusamus terry richardson ad squid. 3 wolf moon officia
                                                            aute,
                                                            non cupidatat skateboard dolor brunch. Food truck quinoa
                                                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                                            aliqua
                                                            put a bird on it squid single-origin coffee nulla assumenda
                                                            shoreditch et. Nihil anim keffiyeh helvetica, craft beer
                                                            labore
                                                            wes anderson cred nesciunt sapiente ea proident. Ad vegan
                                                            excepteur butcher vice lomo. Leggings occaecat craft beer
                                                            farm-to-table, raw denim aesthetic synth nesciunt you
                                                            probably
                                                            haven't heard of them accusamus labore sustainable VHS.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" id="headingTwo" role="tab">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse"
                                                           data-parent="#accordion" href="#collapseTwo"
                                                           aria-expanded="false" aria-controls="collapseTwo"><b>Q.</b> How to
                                                            have
                                                            an editor like this editor here for Q&A plugin? <i
                                                                    class="pull-right fa fa-plus"></i></a>
                                                    </h4>
                                                </div>
                                                <div class="panel-collapse collapse" id="collapseTwo" role="tabpanel"
                                                     aria-labelledby="headingTwo">
                                                    <div class="panel-body pxlr-faq-body">
                                                        <p><b>Answer:</b> Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                            accusamus terry richardson ad squid. 3 wolf moon officia
                                                            aute,
                                                            non cupidatat skateboard dolor brunch. Food truck quinoa
                                                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                                            aliqua
                                                            put a bird on it squid single-origin coffee nulla assumenda
                                                            shoreditch et. Nihil anim keffiyeh helvetica, craft beer
                                                            labore
                                                            wes anderson cred nesciunt sapiente ea proident. Ad vegan
                                                            excepteur butcher vice lomo. Leggings occaecat craft beer
                                                            farm-to-table, raw denim aesthetic synth nesciunt you
                                                            probably
                                                            haven't heard of them accusamus labore sustainable VHS.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" id="headingThree" role="tab">
                                                    <h4 class="panel-title"><a class="collapsed" role="button"
                                                                               data-toggle="collapse"
                                                                               data-parent="#accordion"
                                                                               href="#collapseThree"
                                                                               aria-expanded="false"
                                                                               aria-controls="collapseThree"><b>Q.</b> Problem
                                                            with Knowledge base Article and knowledge base setting? <i
                                                                    class="pull-right fa fa-plus"></i></a></h4>
                                                </div>
                                                <div class="panel-collapse collapse" id="collapseThree" role="tabpanel"
                                                     aria-labelledby="headingThree">
                                                    <div class="panel-body pxlr-faq-body">
                                                        <p><b>Answer:</b> Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                            accusamus terry richardson ad squid. 3 wolf moon officia
                                                            aute,
                                                            non cupidatat skateboard dolor brunch. Food truck quinoa
                                                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                                            aliqua
                                                            put a bird on it squid single-origin coffee nulla assumenda
                                                            shoreditch et. Nihil anim keffiyeh helvetica, craft beer
                                                            labore
                                                            wes anderson cred nesciunt sapiente ea proident. Ad vegan
                                                            excepteur butcher vice lomo. Leggings occaecat craft beer
                                                            farm-to-table, raw denim aesthetic synth nesciunt you
                                                            probably
                                                            haven't heard of them accusamus labore sustainable VHS.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
        <div class="col-md-6">
                                <div class="pxlr-club-faq">
                                    <div class="content">
                                        <div class="panel-group" id="accordion" role="tablist"
                                             aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" id="headingFour" role="tab">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse"
                                                           data-parent="#accordion" href="#collapseFour"
                                                           aria-expanded="false" aria-controls="collapseFour"><b>Q. </b>
                                                            Anonymous user won’t receive
                                                            email private? <i
                                                                    class="pull-right fa fa-plus"></i></a>
                                                    </h4>
                                                </div>
                                                <div class="panel-collapse collapse" id="collapseFour" role="tabpanel"
                                                     aria-labelledby="headingFour">
                                                    <div class="panel-body pxlr-faq-body">
                                                        <p><b>Answer:</b> Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                            accusamus terry richardson ad squid. 3 wolf moon officia
                                                            aute,
                                                            non cupidatat skateboard dolor brunch. Food truck quinoa
                                                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                                            aliqua
                                                            put a bird on it esciunt you probably
                                                            haven't heard of them accusamus labore sustainable VHS.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" id="headingFive" role="tab">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse"
                                                           data-parent="#accordion" href="#collapseFive"
                                                           aria-expanded="false" aria-controls="collapseFive"><b>Q.</b> How to
                                                            have an editor like this editor? 
                                                            <i class="pull-right fa fa-plus"></i>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div class="panel-collapse collapse" id="collapseFive" role="tabpanel"
                                                     aria-labelledby="headingFive">
                                                    <div class="panel-body pxlr-faq-body">
                                                        <p><b>Answer:</b> Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                            accusamus terry richardson ad squid. 3 wolf moon officia
                                                            aute,
                                                            non cupidatat skateboard dolor brunch. Food truck quinoa
                                                            nesciunt laborum eiusmod. ea proident. Ad vegan
                                                            excepteur butcher vice lomo. Leggings occaecat craft beer
                                                            farm-to-table, raw denim aesthetic synth nesciunt you
                                                            probably
                                                            haven't heard of them accusamus labore sustainable VHS.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" id="headingSix" role="tab">
                                                    <h4 class="panel-title"><a class="collapsed" role="button"
                                                                               data-toggle="collapse"
                                                                               data-parent="#accordion"
                                                                               href="#collapseSix" aria-expanded="false"
                                                                               aria-controls="collapseSix"><b>Q.</b> Problem
                                                            with Knowledge base Article? <i
                                                                    class="pull-right fa fa-plus"></i></a></h4>
                                                </div>
                                                <div class="panel-collapse collapse" id="collapseSix" role="tabpanel"
                                                     aria-labelledby="headingSix">
                                                    <div class="panel-body pxlr-faq-body">
                                                        <p><b>Answer:</b> Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                            accusamus terry richardson ad squid. 3 wolf moon officia
                                                            aute,
                                                            non cupidatat skateboard dolor brunch. Food truck quinoa
                                                            nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                                            aliqua
                                                            put a bird on it squid single-origin coffee nulla assumenda
                                                            shoreditch et. Nihil anim keffiyeh helvetica, craft beer
                                                            labore
                                                            wes anderson cred nesciunt sapiente ea proident. Ad vegan
                                                            excepteur butcher vice lomo. Leggings occaecat craft beer
                                                            farm-to-table, raw denim aesthetic synth nesciunt you
                                                            probably
                                                            haven't heard of them accusamus labore sustainable VHS.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
	</div>

	
	<!-- end another way -->
</div>


	 <script type="text/javascript">
	(function (i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] || function () {
		(i[r].q = i[r].q || []).push(arguments)
	}, i[r].l = 1 * new Date();
	a = s.createElement(o),
		m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');


//use trigger click to call the parent class

$(document).ready(function (e) {

	$(".tab_in_sec .panel-default>.panel-heading a").click(function () {
		if ($(this).is('.active')) {
			$(this).removeClass("active");
		} else {
			$(".tab_in_sec .panel-default>.panel-heading a.active").removeClass("active");
			$(this).addClass("active");
		}
	});

	$('.aa').on('click', function () {
		$('#headingOne a').trigger('click');
	});

	$('.bb').on('click', function () {
		$('#headingTwo a').trigger('click');
	});

	$('.cc').on('click', function () {
		$('#headingThree a').trigger('click');
	});

	$('#headingOne').on('click', function () {
		$('.aa').addClass('actives').parent('li').siblings('li').find('a').removeClass('actives');
	});

	$('#headingTwo').on('click', function () {
		$('.bb').addClass('actives').parent('li').siblings('li').find('a').removeClass('actives');
	});

	$('#headingThree').on('click', function () {
		$('.cc').addClass('actives').parent('li').siblings('li').find('a').removeClass('actives');
	});


	$('.tab_first .panel-heading ul li a[data-toggle="collapse"]').on('click', function () {
		$('.tab_first .panel-heading ul li a[data-toggle="collapse"]').removeClass('actives');
		$(this).addClass('actives');
	});


	$('.palceholder').click(function () {
		$(this).siblings('input').focus();
	});
	$('.form-control').focus(function () {
		$(this).siblings('.palceholder').hide();
	});
	$('.form-control').blur(function () {
		var $this = $(this);
		if ($this.val().length == 0)
			$(this).siblings('.palceholder').show();
	});
	$('.form-control').blur();


	$('.tab_first .panel-heading ul li a[href^="#"]').on('click', function (event) {
		var target = $(this.getAttribute('href'));

		if (target.length) {
			event.preventDefault();
			$('html, body').stop().animate({
				scrollTop: target.offset().top - 70
			}, 1000);
		}

	});

});
</script>

@endsection