@extends('layouts.single')

@section('content')
<div class="breadcrumb-w3pvt">
	<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="{{URL::to('/')}}">Home</a>
			</li>
			<li class="breadcrumb-item" aria-current="page">Services</li>
		</ol>
	</nav>
	</div>
</div>
<!-- //breadcrumb -->
		
<!-- services -->
<section class="services py-5">
	<div class="container py-md-5 py-sm-3">
		<h3 class="heading mb-5">Services <strong> we provide </strong></h3>
		<div class="row">
			<div class="col-lg-4">
				<div class="row">

					<div class="col-9 mt-lg-5 grid">
						<a class="linkservice" href="{{route('vas')}}"><h3>VAS</h3></a>
						<p class="mt-2">Nextstep is providing VAS services, Call Center...</p>
					</div>
					<div class="col-3 mt-lg-5 icon text-lg-center text-left grid1">
						<a class="linkservice" href="{{route('vas')}}"><img src="images/a1.png" alt="" class="img-fluid"></a>
					</div>
					
					<div class="col-9 mt-sm-5 mt-4 grid">
						<a class="linkservice" href="{{route('digital_markating')}}"><h3>Digital Marketing</h3></a>
						<p class="mt-2">We are a Nextstep Digital Communication Agency committed to creating...</p>
					</div>
					<div class="col-3 mt-sm-5 mt-4 icon text-lg-center text-left grid4">
						<a class="linkservice" href="{{route('digital_markating')}}"><img src="images/a2.png" alt="" class="img-fluid"></a>
					</div>
					
					<div class="col-9 mt-sm-5 mt-4 grid">
						<a class="linkservice" href="{{route('ecommerce')}}"><h3>E-Commerce</h3></a>
						<p class="mt-2">Comming soon...</p>
					</div>
					<div class="col-3 mt-sm-5 mt-4 icon text-lg-center text-left grid5">
						<a class="linkservice" href="{{route('ecommerce')}}"><img src="images/a3.png" alt="" class="img-fluid"></a>
					</div>
					
				</div>
			</div>
			<div class="col-lg-4 text-center my-lg-0 my-4">
				<img src="images/serv.jpg" alt="" class="img-fluid">
			</div>
			<div class="col-lg-4 text-right">
				<div class="row">
					<div class="col-3 mt-sm-5 mt-4 icon text-lg-center text-right grid6">
						<img src="images/a4.png" alt="" class="img-fluid">
					</div>
					<div class="col-9 mt-sm-5 mt-4 grid">
						<a class="linkservice" href="{{route('import_export')}}"><h3>Export &amp; Import</h3></a>
						<p class="mt-2">Comming soon...</p>
					</div>
					
					<div class="col-3 mt-sm-5 mt-4 icon text-lg-center text-right grid7">
						<a class="linkservice" href="{{route('travel_service')}}"><img src="images/a6.png" alt="" class="img-fluid"></a>
					</div>
					<div class="col-9 mt-sm-5 mt-4 grid">
						<a class="linkservice" href="{{route('travel_service')}}"><h3>Travel Service &amp; Air Ticketing</h3></a>
						<p class="mt-2">Nextstep (pvt) Ltd is one of the best manpower recruiting...</p>
					</div>
					
					<div class="col-3 mt-sm-5 mt-4 icon text-lg-center text-right grid3">
						<a class="linkservice" href="{{route('student_consultancy')}}"><img src="images/a22.png" alt="" class="img-fluid"></a>
					</div>
					<div class="col-9 mt-sm-5 mt-4 grid">
						<a class="linkservice" href="{{route('student_consultancy')}}"><h3>Student Consultancy</h3></a>
						<p class="mt-2">If you are looking to study abroad, want to immigrate...</p>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
<!-- //services -->


		
<!-- products -->

@endsection