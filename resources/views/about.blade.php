@extends('layouts.single')

@section('content')
<div class="breadcrumb-w3pvt">
	<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="{{URL::to('/')}}">Home</a>
			</li>
			<li class="breadcrumb-item" aria-current="page">About Us</li>
		</ol>
	</nav>
	</div>
</div>
<!-- //breadcrumb -->

<!-- advantages and details -->
<section class="advantages pt-5">
	<div class="container pb-lg-5">
		<div class="row advantages_grids">
			<div class="col-lg-6">
				<img src="images/advantages.jpg" alt="" class="img-fluid">
			</div>
			<div class="col-lg-6 mt-lg-0 mt-4">
				<h3 class="">NextStep (pvt.) Ltd. is a private limited company in Bangladesh which have various types of business.</h3>
				<p class="my-sm-4 my-3">Nextstep is a leading Value added service provider company in Bangladesh, has been providing VAS services, Call Center Solution & software development services to the various international and local organizations from Bangladesh.</p>
			</div>
			<div class="col-md-12 nt_about">
				<div>
                    <div class="collapse" id="collapseExample">
	                  	<p class="my-sm-4 my-3">Nextstep student Consultants is one of the best student consultancy firm and travel agencies in Bangladesh.</p>

						<p class="my-sm-4 my-3">Nextstep is working as importers, exporter, shipping agents, commission agent, buying house, selling agent, distributors, brokers, stockiest, wholesalers, retailers, C & F Agents, buyers, sellers and dealers in general merchandise commodities, articles and goods of all description whatsoever.</p>

						<p class="my-sm-4 my-3">NextStep  is an excellent recruiting agency who follow the all professional strategy and procedure to manage workers for placement. </p>

						<p class="my-sm-4 my-3">NextStep is a full service independent advertising agency specialized new-media/digital media which mission to increase brand’s market share, launch new products with a bang and out-innovate the competition.</p>
	                  	</div>
	                </div>
                </div>
			</div>
			<div class="col-lg-6 mt-lg-0 mt-4 nt_about_button">
				<a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><button class="btn btn-primary">Read More</button></a>
			</div>
		</div>
	</div>
</section>
<!-- //advantages and details -->

<!-- testimonials -->
<section class="clients">
	<div class="layer pt-5">
		<div class="container py-lg-5">
			<h2 class="heading mb-sm-5 mb-4">Board <strong> of Directors</strong></h2>
			<div class="row pb-5">
				<div class="col-lg-6 col-md-6 pl-sm-0 mb-3">
					<div class="col- client-grid">
						<div class="c-left">
							<img src="images/bari.jpg" alt="image" class="img-fluid" />
							<div class="info">
								<h6 class="nameee">Md Abdul Bari</h6>
								<p>Chairman, NextStep(Pvt)Ltd</p>
								<li class="my-3"><span class="fa mr-1 fa-phone"></span>+8801849852145</li>
								<li class=""><span class="fa mr-1 fa-envelope"></span><a href="mailto:bari@next-step.com.bd">bari@next-step.com.bd</a></li>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 pl-sm-0 mb-3">
					<div class="col- client-grid">
						<div class="c-left">
							<img src="images/tamim.jpg" alt="image" class="img-fluid" />
							<div class="info">
								<h6 class="nameee">Md Tamim Reza Prodhan</h6>
								<p>Managing Director, NextStep(Pvt)Ltd</p>
								<li class="my-3"><span class="fa mr-1 fa-phone"></span>+8801970216748</li>
								<li class=""><span class="fa mr-1 fa-envelope"></span><a href="mailto:tamim@next-step.com.bd">tamim@next-step.com.bd</a></li>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection