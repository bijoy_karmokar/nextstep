@extends('layouts.single')

@section('content')
<div class="breadcrumb-w3pvt">
	<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="{{URL::to('/')}}">Home</a>
			</li>
			<li class="breadcrumb-item">
				<a href="{{route('service')}}">Service</a>
			</li>
			<li class="breadcrumb-item" aria-current="page">VAS</li>
		</ol>
	</nav>
	</div>
</div>
<!-- //breadcrumb -->
<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:300px;overflow:hidden;visibility:hidden;">
    <!-- Loading Screen -->
    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
    </div>
    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:300px;overflow:hidden;">
        <div>
            <img data-u="image" src="images/vas1.jpg" style="margin-left: 10px;margin-right: 10px;" />
        </div>
        <div>
            <img data-u="image" src="images/vas2.png" style="margin-left: 10px;margin-right: 10px;"/>
        </div>
        <div>
            <img data-u="image" src="images/vas3.jpg" style="margin-left: 10px;margin-right: 10px;"/>
        </div>
    </div>
    <a data-scale="0" href="https://www.jssor.com" style="display:none;position:absolute;">animation</a>
    <!-- Bullet Navigator -->
    <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
            </svg>
        </div>
    </div>
    <!-- Arrow Navigator -->
    <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:35px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
    </div>
    <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:35px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
    </div>
</div>

<section class="advantages pt-5">
	<div class="container pb-lg-5">
		<div class="row advantages_grids">
			<div class="col-lg-8">
				<h3 class="mt-3">Who We Are?</h3>
			</div>
		</div>
		<div class="row advantages_grids">
			<div class="col-lg-8">
				<p class="my-sm-4 my-3">NextStep (Pvt.) Ltd.  Incorporated in Bangladesh is connected to all major cell phone operators, providing value added services through mobile phone network .We are providing exceptional Mobile Vas  and content development, Call Center, SMS, WAP, Website development, Web application development, Software development, Mobile apps & many custom solution.</p>
			</div>
		</div>
	</div>
</section>
<!-- //advantages and details -->

<!-- testimonials -->
<section class="clients">
	<div class="layer pt-5">
		<div class="container py-lg-5">
			<h2 class="heading mb-sm-5 mb-4">Our <strong>KEY SERVICES</strong></h2>
			<div class="row pb-5">
				<div class="col-lg-4 col-md-6 pl-sm-0 mb-3">
					<div class="col- client-grid">
						<div class="c-left">
							<img src="images/smsquiz.jpg" alt="image" class="img-fluid" />
							<div class="info">
								<h6>SMS Quiz</h6>
								<p>- Service</p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							<li class="mt-2">Quiz competition" brings to all Robi customers an exciting SMS Quiz Campaign. Winners will win exciting Gadgets and gifts.</li>
						</div>
						<div class="row">
							<li class="mt-2">ROBI users will be able to participate in the quiz contest by registering ‘General Knowledge’ service by sending “START GK” to 16696.</li>
						</div>
						<div class="row">
							<li class="mt-2">User will have to participate by answering the first question and will get the next one.</li>
						</div>
						<div class="row">
							<li class="mt-2">For answering questions you need to type A/B and send to 16696. If the answer is received, the system shall send the next question in return by mentioning the Right/Wrong of the previous question.</li>
						</div>
						<div class="row">
							<li class="mt-2">Answer Reply by A/B Charge: 2.55 BDT/SMS (Including SD, SC & VAT).</li>
						</div>
						<div class="row">
							<li class="mt-2">Notification/Confirmation message & Opt-in/opt-out messages, Question, Score: FREE.</li>
						</div>
						<div class="row">
							<li class="mt-2">1st  Prize : Redmi Note 8.</li>
						</div>
						<div class="row">
							<li class="mt-2">2nd Prize : Redmi 8.</li>
						</div>
						<div class="row">
							<li class="mt-2">3rd Prize : Symphony i75.</li>
						</div>
						<div class="row">
							<li class="mt-2">After 6 Month who will reached benchmark, he/she will get bellow prize.</li>
						</div>
						<div class="row">
							<li class="mt-2">Honda Livo.</li>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 pl-sm-0 mb-3">
					<div class="col- client-grid">
						<div class="c-left">
							<img src="images/friend-ly.jpg" alt="image" class="img-fluid" />
							<div class="info">
								<h6>WAP/APP Content</h6>
								<p>- Service</p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							<li class="mt-2">"Friend.ly” is a APP based entertainment service platform which is amazing offering community management opportunity for this generation. A service that will help you to build a new relationship with new people, communicate with each other.</li>
						</div>
						<div class="row">
							<li class="mt-2">"Friend.ly" is Android, iOS and Windows mobile platform at same time.</li>
						</div>
						<div class="row">
							<li class="mt-2">Find a friend, Make a friend, Manage My profile, Chat with friend.</li>
						</div>
						<div class="row">
							<li class="mt-2">This service is only available for Robi and Airtel users.</li>
						</div>
						<div class="row">
							<li class="mt-2">Daily subscription charge is 2.55 taka.</li>
						</div>
						<div class="row">
							<li class="mt-2">Internet is a must to use this App.</li>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 pl-sm-0 mb-3">
					<div class="col- client-grid">
						<div class="c-left">
							<img src="images/ludo.png" alt="image" class="img-fluid" />
							<div class="info">
								<h6>Online Game</h6>
								<p>- Service</p>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							<li class="mt-2">"BanglarLudo"  is a classic board game played between friends and family.  This game is surely recall user's childhood!</li>
						</div>
						<div class="row">
							<li class="mt-2">"BanglarLudo" is a cross platform multiplayer game that supports Desktop, Android, iOS and Windows mobile platform at same time. This game also support offline mode, where player can play with Computer or, Local multiplayer (play and pass mode).</li>
						</div>
						<div class="row">
							<li class="mt-2">Best scorers will receive attractive gifts like Air time amount, Data pack etc.</li>
						</div>
						<div class="row">
							<li class="mt-2">Play with your family and friends through Local and Online Multiplayer.</li>
						</div>
						<div class="row">
							<li class="mt-2">Play 2 to 4 Player Local Multiplayer Mode.</li>
						</div>
						<div class="row">
							<li class="mt-2">Express yourself by sending emojis to your opponents.</li>
						</div>
						<div class="row">
							<li class="mt-2">Graphics with a classic look and the feel of a dice game.</li>
						</div>
						<div class="row">
							<li class="mt-2">Invite and challenge your Facebook Friends in a private game room and beat them.</li>
						</div>
						<div class="row">
							<li class="mt-2">Leader board.</li>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection