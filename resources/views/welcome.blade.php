@extends('layouts.main')

@section('content')
<section class="advantages hpage pt-lg-5 pt-3">
    <div class="container pb-lg-5">
        <div class="row advantages_grids">
            <div class="col-lg-6">
                <img src="{{URL::asset('images/advantages.jpg')}}" alt="" class="img-fluid">
            </div>
            <div class="col-lg-6 mt-lg-0 mt-4">
                <h3 class="mt-3">NextStep (Pvt.) Ltd. Is A Private Limited Company In Bangladesh Which Have Various Types Of Business.</h3>
                <p class="my-sm-4 my-3">  Nextstep is a leading Value added service provider company in Bangladesh, has been providing VAS services, Call Center Solution & software development services to the various international and local organizations from Bangladesh.</p>
            </div>
            
        </div>
    </div>
</section>
<!-- //advantages and details -->

<!-- statistics -->
<!-- <section class="statistics py-5">
    <div class="container py-lg-5 py-sm-3">
        <h3 class="heading mb-sm-5 mb-4">Latest <strong>statistical information</strong></h3>
        <div class="row">
            <div class="col-lg-4 mb-lg-0 mb-5">
                <p class="mb-4">  Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere Curae Integer ullati mcorper metus et urna
                maximus, ac maximus estlen blandit. Nam pretium tempor mi, a sed fringilla leo imperdiet intre. Integer lacinia et ultricies turpis.
                non euismod lectus tincidunt vitae. Quisqe non facilisis ante.</p>
                <a href="#">Read more </a>
            </div>
            <div class="col-lg-4 col-md-6 mb-lg-0 mb-4">
                <img src="images/girl.png" alt="" class="img-fluid">
            </div>
            <div class="col-lg-4 col-md-6 mt-lg-0 mt-md-4">
                <div class="progress-one">
                    <h4 class="progress-tittle">Online Banking</h4>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="progress-one my-lg-4">
                    <h4 class="progress-tittle">Financial Consulting</h4>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped bg-danger" role="progressbar" style="width: 55%" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="progress-one">
                    <h4 class="progress-tittle">Credit and debit cards</h4>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="progress-one my-lg-4">
                    <h4 class="progress-tittle">Existing Loans</h4>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="progress-one">
                    <h4 class="progress-tittle">24/7 support</h4>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 90%" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<!-- //statistics -->
<!-- about -->
<section class="banner-bottom pt-5">
    <div class="container">
        <h3 class="heading mb-sm-5 mb-4">Our <strong>Services</strong></h3>
        <div class="row bottom_grids text-center mt-lg-5 mt-sm-3">
            <div class="col-md-4 grid1 mb-5">
                <a href="{{route('vas')}}">
                <img src="images/a1.png" alt="" class="img-fluid">
                <h3 class="my-3">VAS</h3>
                <p class=""> Nextstep is providing VAS services, Call Center Solution & software development services to the various international and local organizations from Bangladesh</p>
            </a>
            </div>
            <div class="col-md-4 grid2 mb-5">
                <a href="{{route('digital_markating')}}">
                <img src="images/a2.png" alt="" class="img-fluid">
                <h3 class="my-3">Digital Marketing</h3>
                <p class="">We are a Nextstep Digital Communication Agency committed to creating an actionable strategy, online marketing & technology solution for our partners.</p>
            </a>
            </div>
            <div class="col-md-4 grid3 mb-5">
                <a href="{{route('ecommerce')}}">
                <img src="images/a3.png" alt="" class="img-fluid">
                <h3 class="my-3">E-Commerce</h3>
                <p class=""> Vestibulum ante ipsum primis in faucibus orci luctus eted ultrices posuere Curae primis in faucibus orci luctus eted.</p>
            </a>
            </div>
        </div>
        <div class="row bottom_grids text-center mt-lg-5 mt-sm-3">
            <div class="col-md-4 grid1 mb-5">
                <a href="{{route('import_export')}}">
                <img src="images/a4.png" alt="" class="img-fluid">
                <h3 class="my-3 aaaaa">Export &amp; Import</h3>
                <p class="">We is working as importers, exporter, shipping agents, commission agent, buying house, selling agent, distributors, and wholesalers.</p>
            </a>
            </div>
            <div class="col-md-4 grid2 mb-5">
                <a href="{{route('travel_service')}}">
                <img src="images/a6.png" alt="" class="img-fluid">
                <h3 class="my-3 aaaaa">Travel Service &amp; Air Ticketing</h3>
                <p class="">Nextstep (pvt) Ltd is one of the best manpower recruiting company in Bangladesh, known for its professional and excellence service.</p>
            </a>
            </div>
            <div class="col-md-4 grid3 mb-5">
                <a href="{{route('student_consultancy')}}">
                <img src="images/a22.png" alt="" class="img-fluid">
                <h3 class="my-3 aaaaa">Student Consultancy</h3>
                <p class="">If you are looking to study abroad, want to immigrate or looking visitor visa, business visa, you’ve come to the right place.</p>
            </a>
            </div>
        </div>
    </div>
</section>
<!-- //about -->

<!-- products -->
<section class="products py-5">
    <div class="container py-lg-5 py-3">
        <h3 class="heading mb-sm-5 mb-4">Our <strong>Products</strong></h3>
        <div class="row products_grids text-center mt-5">
            <div class="col-md-3 col-6 grid4">
                <div class="prodct1 border p-3">
                    <a href="{{route('friendly')}}">
                        <img src="images/app/friendly/friendly_app_icon.png" alt="" class="img-fluid">
                        <h3 class="mt-2">Friendly</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
            <!-- <div class="col-md-3 col-6 grid5">
                <div class="prodct1 border p-3">
                    <a href="#">
                        <img src="images/a2.png" alt="" class="img-fluid">
                        <h3 class="mt-2">Eshop</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div> -->
            <div class="col-md-3 col-6 grid6 mt-md-0 mt-3">
                <div class="prodct1 border p-3">
                    <a href="{{route('flappycash')}}">
                        <img src="images/app/fcash/flappy_app_icon.png" alt="" class="img-fluid">
                        <h3 class="mt-2">FlappyCash</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-6 grid7 mt-md-0 mt-3">
                <div class="prodct1 border p-3">
                    <a href="{{route('ludo')}}">
                        <img src="images/app/ludo/app3.png" alt="" class="img-fluid">
                        <h3 class="mt-2">LudoQueen</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //products -->

<!-- stats section -->
@endsection