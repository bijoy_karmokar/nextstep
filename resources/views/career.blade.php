@extends('layouts.single')

@section('content')
<div class="breadcrumb-w3pvt">
    <div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{URL::to('/')}}">Home</a>
            </li>
            <li class="breadcrumb-item" aria-current="page">Career</li>
        </ol>
    </nav>
    </div>
</div>


<div class="container py-md-5 py-sm-3">
    
    <div class="row">
        <div class="col-6">
            <div class="">
                <h1 class="heading mb-5">
                    No Opportunities available
                </h1>
                <!-- <p><strong>Job Responsibilities</strong></p>
                <li>Handle Group Accounts & Finance.</li>
                <li>Monitor and direct the implementation of strategic business plans of Group (all sister concern).</li>
                <li>Manage the capital request and budgeting processes.</li>
            </div>  
            <div class="button-box">
                <a class="button" href="#popup1">View More</a>
            </div> -->
            </div>
        </div>
    </div>
    
</div>

<!-- <section class="services py-5">
    <div class="container py-md-5 py-sm-3" id="career">
        <div id="popup1" class="overlay">
            <div class="popup">
                <h4 class="heading mb-5">
                    Sr. GM / CFO (Chief Financial Officer) 
                </h4>
                <a class="close" href="">&times;</a>
                <p><strong>Job Responsibilities</strong></p>
                <li>Handle Group Accounts & Finance.</li>
                <li>Monitor and direct the implementation of strategic business plans of Group (all sister concern).</li>
                <li>Manage the capital request and budgeting processes.</li>
                <li>Manage the capital request and budgeting processes.</li>
                <li>Develop performance measures that support the company's strategic direction.</li>
                <li>Oversee day to day Financial operations of all sister concern of Group to comply Board of the Director.</li>
                <li>Participate in key decisions as a member of the executive management team of all sister concern of Group.</li>
                <li>Maintain in-depth relations with all members of the management team.</li>
                <li>Manage the accounting, revenue collection, billing, banks, investor relations, legal, tax, and treasury departments.</li>
                <li>Oversee the financial operations of subsidiary companies and foreign operations.</li>
                <li>Any other job as assigned by Management.</li>
                <br>
                <h4>Employment Status</h4>
                <p>Full-time</p>
                <br>
                <h4>Educational Requirements</h4>
                <li>Bachelor of Commerce (BCom), Master of Commerce (MCom)</li>
                <li>ICAB, ICMA</li>
                <li>Skills Required: Accounts and Finance, Group of Companies</li>
                <br>
                <h4>Experience Requirements</h4>
                <li>At least 10 year(s)</li>
                <li>The applicants should have experience in the following area(s):</li>
                <li>Group of Companies</li>
                <li>The applicants should have experience in the following business area(s):</li>
                <li>Battery, Storage cell, Jute Goods/ Jute Yarn, Manufacturing (Light Engineering & Heavy Industry)</li>
                <br>
                <h4>Additional Requirements</h4>
                <li>Age at least 35 years</li>
                <br>
                <h4>Job Location</h4>
                <li>Anywhere in Bangladesh</li>
                <br>
                <h4>Salary</h4>
                <li>Negotiable</li>
                <br>
                <h4>Compensation & Other Benefits</h4>
                <li>T/A, Mobile bill, Tour allowance, Provident fund</li>
                <li>Lunch Facilities: Partially Subsidize</li>
                <li>Salary Review: Yearly</li>
                <li>Festival Bonus: 2</li>
                <p><strong>Feel free to send your cv to</strong>:  <ul>info@mail.com</ul></p>
            </div>
            
        </div>
    </div>
</section> -->

@endsection