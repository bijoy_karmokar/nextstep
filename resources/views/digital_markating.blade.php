@extends('layouts.single')

@section('content')
<div class="breadcrumb-w3pvt">
	<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
                <a href="{{URL::to('/')}}">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{route('service')}}">Service</a>
            </li>
            <li class="breadcrumb-item" aria-current="page">Digital Markating</li>
		</ol>
	</nav>
	</div>
</div>
<!-- //breadcrumb -->

<!-- advantages and details -->
<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:300px;overflow:hidden;visibility:hidden;">
    <!-- Loading Screen -->
    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
    </div>
    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:300px;overflow:hidden;">
        <div>
            <img data-u="image" src="images/dm1.jpg" style="margin-left: 10px;margin-right: 10px;"/>
        </div>
        <div>
            <img data-u="image" src="images/dm.jpg" style="margin-left: 10px;margin-right: 10px;"/>
        </div>
    </div>
    <a data-scale="0" href="https://www.jssor.com" style="display:none;position:absolute;">animation</a>
    <!-- Bullet Navigator -->
    <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
            </svg>
        </div>
    </div>
    <!-- Arrow Navigator -->
    <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:35px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
    </div>
    <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:35px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
    </div>
</div>
<section class="advantages pt-5">
	<div class="container pb-lg-5">
		<div class="row advantages_grids">
			<div class="col-lg-8">
				<h3 class="mt-3">NextStep (pvt.) Ltd. is a private limited company in Bangladesh which have various types of business. .</h3>
			</div>
		</div>
		<div class="row advantages_grids">
			<div class="col-lg-12">
				<p class="my-sm-4 my-3">We are a Nextstep Digital Communication Agency committed to creating an actionable strategy, online marketing & technology solution for our partners. We have brought strategy design and technology to help businesses thrive in rapidly changing market. We have expert team who is very much energetic and skillful. Now at present without advertising your product can not reach the clients that`s why we have promoted your product to the client so that you can get so many audiences. We do research and find the truth behind a brand and find transformative ideas that seduce the heart of consumer.</p>
			</div>
		</div>
	</div>
</section>
<section class="products py-5">
    <div class="container py-lg-5 py-3">
        <h3 class="heading mb-sm-5 mb-4">Our <strong>Products</strong></h3>
        <div class="row products_grids text-center mt-5">
            <div class="col-md-3 col-6 grid4">
                <div class="prodct1 border p-3">
                    <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <img src="images/em.png" alt="" class="img-fluid">
                        <h3 class="mt-2">Email Marketing</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                    <div class="collapse" id="collapseExample">
	                  	<div class="card card-body">
	                    	We will send this email by our software and you will get access to see the LIVE report of how much email we are sending. This report shows the email count only. We need 2/3 days to complete the full email marketing. You have to provide the design in text or html or .jpg picture. We also need your email subject, reply email address.
	                  	</div>
	                </div>
                </div>
            </div>
            <div class="col-md-3 col-6 grid5">
                <div class="prodct1 border p-3">
                    <a data-toggle="collapse" href="#smscollapseExample" role="button" aria-expanded="false" aria-controls="smscollapseExample">
                        <img src="images/sm.png" alt="" class="img-fluid">
                        <h3 class="mt-2">Sms Marketing</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                    <div class="collapse" id="smscollapseExample">
	                  	<div class="card card-body">
	                    	We have lot of mobile numbers of different corporate people or personal where you can send sms.. You can send any amount of SMS. Generally 98% people read SMS. So this is one of the best marketing procedures to reach the target people.
	                  	</div>
	                </div>
                </div>
            </div>
            <div class="col-md-3 col-6 grid6 mt-md-0 mt-3">
                <div class="prodct1 border p-3">
                    <a data-toggle="collapse" href="#fbcollapseExample" role="button" aria-expanded="false" aria-controls="fbcollapseExample">
                        <img src="images/fbm.png" alt="" class="img-fluid">
                        <h3 class="mt-2">Facebook Marketing</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                    <div class="collapse" id="fbcollapseExample">
	                  	<div class="card card-body">
	                    	Now a day Facebook marketing is very important. You can easily reach too many people by Facebook. Just some simple procedure we follow. Create a company page on Facebook. Write some imporatnt and attractive article for facebook. Start advertisement and make it viral in facebook. We advertise for page like so that once one connect with your page, can get notification regularly.
	                  	</div>
	                </div>
                </div>
            </div>
            <div class="col-md-3 col-6 grid7 mt-md-0 mt-3">
                <div class="prodct1 border p-3">
                    <a data-toggle="collapse" href="#ytmcollapseExample" role="button" aria-expanded="false" aria-controls="ytmcollapseExample">
                        <img src="images/ytm.jpg" alt="" class="img-fluid">
                        <h3 class="mt-2">Youtube Marketing</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                    <div class="collapse" id="ytmcollapseExample">
	                  	<div class="card card-body">
	                    	If you want to do video marketing then youtube is the best way. Any type of video will start after your 5 second video advertisement.
	                  	</div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection