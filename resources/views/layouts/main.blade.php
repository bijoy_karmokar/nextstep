<!DOCTYPE html>
<html lang="en">
<head>
<title>Home</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);		
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
	
	<!-- css files -->
    <link href="css/css_slider.css" rel="stylesheet"><!-- Slider css -->
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' /><!-- bootstrap css -->
    <link href="css/style.css" rel='stylesheet' type='text/css' /><!-- custom css -->
    <link href="css/font-awesome.min.css" rel="stylesheet"><!-- fontawesome css -->
    <link href="css/oneby.css" rel="stylesheet"><!-- fontawesome css -->
	<!-- //css files -->
	
	<!-- google fonts -->
	<link href="//fonts.googleapis.com/css?family=Niramit:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext,thai,vietnamese" rel="stylesheet">
	<!-- //google fonts -->
	 <!-- <link href="css/freelancer.css" rel="stylesheet"> -->
	     <!-- <link href="css/bootslide.css" rel='stylesheet' type='text/css' /> -->
	     <!-- bootstrap css -->

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="js/oneby.js"></script>

  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <script src="js/freelancer.min.js"></script>
    <script src="js/jssor.slider-28.0.0.min.js" type="text/javascript"></script>
    
    <link href="//fonts.googleapis.com/css?family=Montserrat:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic&subset=latin-ext,cyrillic-ext,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
    <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,regular,italic,700,700italic&subset=latin-ext,greek-ext,cyrillic-ext,greek,vietnamese,latin,cyrillic" rel="stylesheet" type="text/css" />
  

<script>
$(function(){
    $('#obo_slider').oneByOne({
		className: 'oneByOne1',	             
		easeType: 'random',
		slideShow: true
	});  
})
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();
</script>

	
</head>
<body id="page-top">
	<header>
<!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
    <div class="container">
      <!-- <a  href="#page-top">Start Bootstrap</a> -->
      <a href="{{URL::to('/')}}" class="navbar-brand js-scroll-trigger" ><img src="images/selicelogo.png" class="img-fluid fld"></a>

      <button class=" mobmenu navbar-toggler navbar-toggler-right text-uppercase font-weight-bold text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mobnavsc ml-auto">
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{URL::to('/')}}">Home</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{route('about')}}">About</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{route('service')}}">Services</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{route('career')}}">Career</a>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="{{route('contact')}}">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
<!-- one bt one slider -->
<div class="wrape homeone">
		<!-- <div class="fallback"><img src="img/os.jpg" alt="" /></div> -->
		<div id="obo_slider">
        <!-- <div class="oneByOne_item">
            <span class="txt1">Mobile Recharge at</span><br>
            <span class="txt3"><a href="http://ezpaybd.com/" target="_blank"><img style="border:1px solid #FFF;" src="img/os.jpg" height="76" width="387"></a></span>
			<span class="txt3 short">Online mobile phone recharge system in Bangladesh.Where you can FlexiLoad or recharge your Mobile Account by using your AlertPay, Skrill (Moneybookers) and Liberty Reserve Account. It's easy and simple.</span>												
			
		</div>  				
		<div class="oneByOne_item">
			<img src="img/sss.png" class="wp1_3 slide1_bot" alt="">		            
			<span class="txt1">Ring Back Tone</span>			
			<span class="txt2" style="font-size:28px;">RBT</span>												
			<span class="txt3 short">Wintel has a very rich Music content bank which comprises of Thirty thousands Bengali music, Two thousands Hindi music and twelve hundred English music from 87 licensed Music production house and content partners.</span>												
			
		</div>
		
		<div class="oneByOne_item">
			<img src="img/sss.png" class="wp1_3 slide2_bot" alt="">		            			
            <span class="txt1">Wireless Application Protocol</span>
			<span class="txt2" style="font-size:28px;">WAP</span>												
			<span class="txt3">Through GPRS and EDGE technology we are also providing WAP contents like Image, Animated Image, Ring Tones, SMS Tones, Color Logo, Theme, J2ME Games & Application, Video Clips, etc.</span>												
			
		</div>                                                                                              
		<div class="oneByOne_item">                                 	
			<img src="img/sss.png" class="wp1_3  slide2_bot" alt="">			            
            <span class="txt1">Interactive Voice Response</span>
			<span class="txt2" style="font-size:28px;">IVR</span>												
			<span class="txt3">Wintel is providing SMS broadcast service to various corporate houses, Government Organizations, Educational institutions, business & Social communities etc.</span>			
			
		</div>	
		<div class="oneByOne_item">                                 	
			<img src="img/sss.png" class="wp1_3 slide3_bot" alt="">
            <span class="txt1"> &nbsp;</span>			            		
			<span class="txt2" style="font-size:28px;">Corporate Service</span>												
			<span class="txt3 short">Wintel is providing SMS broadcast service to various corporate houses, Government Organizations, Educational institutions, business & Social communities etc.</span>												
			
		</div> -->
		<div class="oneByOne_item">
			<img src="images/hps.png" class="wp1_3 slide1_bot" alt="">		            
            <span class="txt1">&nbsp;</span>
			<span class="txt2" style="font-size:28px;">VAS services</span>												
			<span class="txt3 short">
			Nextstep is providing VAS services, Call Center Solution & software development services to the various international and local organizations from Bangladesh
			</span>												
		</div>
		<div class="oneByOne_item">
			<img src="images/hps1.png" class="wp1_3 slide1_bot" alt="">		            
            <span class="txt1">&nbsp;</span>
			<span class="txt2" style="font-size:28px;">Student Consultancy</span>												
			<span class="txt3 short">
				
If you are looking to study abroad, want to immigrate or looking visitor visa, business visa, you’ve come to the right place. We at Nextstep one of the best visa consultancy in Bangladesh.

			</span>												
		</div>
		<div class="oneByOne_item">
			<img src="images/hps2.png" class="wp1_3 slide1_bot" alt="">		            
            <span class="txt1">&nbsp;</span>
			<span class="txt2" style="font-size:28px;">Excellent Recruitement</span>												
			<span class="txt3 short">NextStep is an excellent recruiting agency who follow the all professional strategy and procedure to manage workers for placement. </span>												
		</div>
		<div class="oneByOne_item">
			<img src="images/hps3.png" class="wp1_3 slide1_bot" alt="">		            
            <span class="txt1">&nbsp;</span>
			<span class="txt2" style="font-size:28px;">Export and Import</span>												
			<span class="txt3 short">We is working as importers, exporter, shipping agents, commission agent, buying house, selling agent, distributors, and wholesalers</span>												
		</div>
		<div class="oneByOne_item">
			<img src="images/hps4.png" class="wp1_3 slide1_bot" alt="">		            
            <span class="txt1">&nbsp;</span>
			<span class="txt2" style="font-size:28px;">Upcoming Online Shopping</span>												
			<span class="txt3 short">
				Nextstep is upcoming online shopping in Bangladesh with free home delivery. 
			</span>												
		</div>
		<div class="oneByOne_item">
			<img src="images/hps5.png" class="wp1_3 slide1_bot" alt="">		            
            <span class="txt1">&nbsp;</span>
			<span class="txt2" style="font-size:28px;">Digital Marketing</span>												
			<span class="txt3 short">NextStep is a full service independent advertising agency who believe in real time innovation, quantifiable result and lasting impression</span>												
		</div>

		</div>
		        
</div>
<!-- /one bt one slider -->



</header>


@yield('content')
<footer class="footer py-5">
	<div class="container pt-lg-4">
		<div class="row">
			<div class="col-lg-4 col-sm-6 footer-top">
				<h4 class="mb-4 w3f_title">Contact Info</h4>
				<ul class="list-w3">
					<li><span class="fa mr-1 fa-map-marker"></span>House#31, Road#21, <br>Nikunju-2, Dhaka-1229</li>
					<li class="my-2"><span class="fa mr-1 fa-phone"></span>+8801799382548</li>
					<li class="my-2"><span class="fa mr-1 fa-phone"></span>+8801799382549</li>
					<li class=""><span class="fa mr-1 fa-envelope"></span><a href="mailto:info@next-step.com.bd">info@next-step.com.bd</a></li>
				</ul>
			</div>
			<div class="col-lg-4 col-sm-6 footv3-left mt-sm-0 mt-4">
				<h4 class="mb-4 w3f_title"> Quick Access</h4>
				<ul class="list-w3">
					<li class="my-2">
						<a href="{{URL::to('/')}}">
							Home
						</a>
					</li>
					<li class="mb-2">
						<a href="{{route('about')}}">
							About
						</a>
					</li>
					<li class="my-2">
						<a href="{{route('service')}}">
							Services
						</a>
					</li>
					
					<li>
						<a href="{{route('contact')}}">
							Contact
						</a>
					</li>
				</ul>
			</div>
			
			<div class="col-lg-4 col-sm-6 mt-lg-0 mt-sm-5 mt-4">
				<h4 class="mb-4 w3f_title">Other Links</h4>
				<ul class="list-w3">
					<li class="my-2">
						<a href="{{route('career')}}">
							Careers
						</a>
					</li>
					<li class="mb-2">
						<a href="{{route('faq')}}">
							All faq's
						</a>
					</li>
				</ul>
			</div>
			
		</div>
	</div>
	<!-- //footer bottom -->
</footer>
<!-- //footer -->



<!-- copyright -->
<section class="copy-right py-4">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<p class="">© 2019 All rights reserved | Design by
					<a href="#"> CyberBlaze Technologies.</a>
				</p>
			</div>
			<div class="col-lg-5 mt-lg-0 mt-3">
				<ul class="list-w3 d-sm-flex">
					<li>
						<a href="#">
							Privicy Policy
						</a>
					</li>
					<li class="mx-sm-4 mx-3">
						<a href="#">
							Terms & Conditions
						</a>
					</li>
					<li>
						<a href="#">
							Disclaimer.
						</a>
					</li>
					<li>
						<a href="#">
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- //copyright -->

<!-- move top -->
<div class="move-top text-right">
	<a href="#home" class="move-top"> 
		<span class="fa fa-angle-up  mb-3" aria-hidden="true"></span>
	</a>
</div>
<!--  <script type="text/javascript">jssor_1_slider_init();
    </script> -->
<!-- move top -->
