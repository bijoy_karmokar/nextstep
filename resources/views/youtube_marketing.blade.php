@extends('layouts.single')

@section('content')
<div class="breadcrumb-w3pvt">
	<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="index.html">Home</a>
			</li>
			<li class="breadcrumb-item" aria-current="page">Youtube marketing</li>
		</ol>
	</nav>
	</div>
</div>
<!-- //breadcrumb -->

<!-- advantages and details -->
<section class="advantages pt-5">
	<div class="container pb-lg-5">
		<div class="row advantages_grids">
			<div class="col-lg-6">
				<img src="images/dm.jpg" alt="" class="img-fluid">
			</div>
			<div class="col-lg-6 mt-lg-0 mt-4">
				<h3 class="mt-3">NextStep (pvt.) Ltd. is a private limited company in Bangladesh which have various types of business. .</h3>
				<p class="my-sm-4 my-3">If you want to do video marketing then youtube is the best way. Any type of video will start after your 5 second video advertisement.</p>	
			</div>
			<!-- <div class="row mt-md-5 mt-4 image-grids">
				<div class="col-lg-3 col-6 grid4 mb-5">
					<img src="images/a1.png" alt="" class="img-fluid">
					<h3 class="mt-3">Email Marketing</h3>
					<p class=""> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices.</p>
				</div>
				<div class="col-lg-3 col-6 grid5 mb-5">
					<img src="images/a2.png" alt="" class="img-fluid">
					<h3 class="mt-3">SMS Marketing</h3>
					<p class=""> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices.</p>
				</div>
				<div class="col-lg-3 col-6 grid6 mb-5">
					<img src="images/a3.png" alt="" class="img-fluid">
					<h3 class="mt-3">Facebook Marketing</h3>
					<p class=""> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices.</p>
				</div>
				<div class="col-lg-3 col-6 grid7 mb-5">
					<img src="images/a2.png" alt="" class="img-fluid">
					<h3 class="mt-3">YouTube Marketing</h3>
					<p class=""> Vestibulum ante ipsum primis in faucibus orci luctus et ultrices.</p>
				</div>
			</div> -->
		</div>
	</div>
</section>
<section class="products py-5">
    <div class="container py-lg-5 py-3">
        <h3 class="heading mb-sm-5 mb-4">Our <strong>Products</strong></h3>
        <div class="row products_grids text-center mt-5">
            <div class="col-md-3 col-6 grid4">
                <div class="prodct1 border p-3">
                    <a href="{{route('email_marketing')}}">
                        <img src="images/a1.png" alt="" class="img-fluid">
                        <h3 class="mt-2">Email Marketing</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-6 grid5">
                <div class="prodct1 border p-3">
                    <a href="{{route('sms_marketing')}}">
                        <img src="images/a2.png" alt="" class="img-fluid">
                        <h3 class="mt-2">SMS Marketing</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-6 grid6 mt-md-0 mt-3">
                <div class="prodct1 border p-3">
                    <a href="{{route('facebook_marketing')}}">
                        <img src="images/a3.png" alt="" class="img-fluid">
                        <h3 class="mt-2">Facebook Marketing</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-6 grid7 mt-md-0 mt-3">
                <div class="prodct1 border p-3">
                    <a href="{{route('youtube_marketing')}}">
                        <img src="images/a22.png" alt="" class="img-fluid">
                        <h3 class="mt-2">YouTube Marketing</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection