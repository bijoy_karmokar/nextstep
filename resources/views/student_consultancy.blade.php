@extends('layouts.single')

@section('content')
<div class="breadcrumb-w3pvt">
	<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="{{URL::to('/')}}">Home</a>
			</li>
			<li class="breadcrumb-item">
				<a href="{{route('service')}}">Service</a>
			</li>
			<li class="breadcrumb-item" aria-current="page">Student Consultancy</li>
		</ol>
	</nav>
	</div>
</div>
<!-- //breadcrumb -->
<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:300px;overflow:hidden;visibility:hidden;">
    <!-- Loading Screen -->
    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/spin.svg" />
    </div>
    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:300px;overflow:hidden;">
        <div>
            <img data-u="image" src="images/sc.png" style="margin-left: 10px;margin-right: 10px;" />
        </div>
        <div>
            <img data-u="image" src="images/sc1.jpg" style="margin-left: 10px;margin-right: 10px;"/>
        </div>
        <div>
            <img data-u="image" src="images/sc2.jpg" style="margin-left: 10px;margin-right: 10px;"/>
        </div>
    </div>
    <a data-scale="0" href="https://www.jssor.com" style="display:none;position:absolute;">animation</a>
    <!-- Bullet Navigator -->
    <div data-u="navigator" class="jssorb051" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
            </svg>
        </div>
    </div>
    <!-- Arrow Navigator -->
    <div data-u="arrowleft" class="jssora051" style="width:65px;height:65px;top:0px;left:35px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
    </div>
    <div data-u="arrowright" class="jssora051" style="width:65px;height:65px;top:0px;right:35px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
    </div>
</div>
<section class="advantages pt-5">
    <div class="container pb-lg-5">
        <div class="row advantages_grids">
            <div class="col-lg-8">
                <h3 class="mt-3">Welcome to Nextstep student consultancy.</h3>
            </div>
        </div>
        
        <div class="row advantages_grids">
            <div class="col-lg-12">
                <p class="my-sm-4 my-3">If you are looking to study abroad, whether it’s high school, college or university, you’ve come to the right place. We at Nextstep student consultancy pride ourselves in being one of the best student visa agencies in Bangladesh. Consultancy firms can play the most important role to serve you proper knowledge and assist you to get admission and visa.</p>
            </div>
        </div>

      	<div class="ef">
      		<div class="row">
      			<div class="col-md-3">
      				<div class="ef-box">
		          		<img src="images/Canada.jpg">
		          		<div class="sweep-to-top" id="s1">
				          	<div class="animation-content a1" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>

      			<div class="col-md-3">
      				<div class="ef-box ">
		          		<img src="images/Germany.png">
						<div class="sweep-to-top" id="s2">
				          	<div class="animation-content a2" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>

      			<div class="col-md-3">
      				<div class="ef-box ">
		          		<img src="images/China.jpg">
		          		<div class="sweep-to-top" id="s3">
				          	<div class="animation-content a3" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>

      			<div class="col-md-3">
      				<div class="ef-box">
		          		<img src="images/Finland.png">
		          		<div class="sweep-to-top" id="s4">
				          	<div class="animation-content a4" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>
      		</div>
      		
      		<div class="row">
      			<div class="col-md-3">
      				<div class="ef-box">
		          		<img src="images/Hungary.jpg">
		          		<div class="sweep-to-top" id="s5">
				          	<div class="animation-content a5" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>

      			<div class="col-md-3">
      				<div class="ef-box ">
		          		<img src="images/Italy.png">
						<div class="sweep-to-top" id="s6">
				          	<div class="animation-content a6" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>

      			<div class="col-md-3">
      				<div class="ef-box ">
		          		<img src="images/Malta.png">
		          		<div class="sweep-to-top" id="s7">
				          	<div class="animation-content a7" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>

      			<div class="col-md-3">
      				<div class="ef-box">
		          		<img src="images/Poland.jpg">
		          		<div class="sweep-to-top" id="s8">
				          	<div class="animation-content a8" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>
		    </div>
      		
      		<div class="row">
      			<div class="col-md-3">
      				<div class="ef-box">
		          		<img src="images/Portugal.png">
		          		<div class="sweep-to-top" id="s9">
				          	<div class="animation-content a9" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>

      			<div class="col-md-3">
      				<div class="ef-box ">
		          		<img src="images/Sweden.jpg">
						<div class="sweep-to-top" id="s10">
				          	<div class="animation-content a10" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>

      			<div class="col-md-3">
      				<div class="ef-box ">
		          		<img src="images/USA.jpg">
		          		<div class="sweep-to-top" id="s11">
				          	<div class="animation-content a11" style="display: none;">
			          			<h5><span>Call </span>to</h5>
			                    <a class="fa fa-phone" href="tel:+8801799-382548">+8801799-382548</a>
			                </div>
			          	</div>
		          	</div>
		        </div>
			</div>
      	</div>
    </div>
</section>
<!-- advantages and details -->
<!-- <section class="products py-5">
    <div class="container py-lg-5 py-3">
        <h3 class="heading mb-sm-5 mb-4">Study <strong>Abroad</strong></h3>
        <div class="row products_grids text-center mt-5">
    
        <script type="text/javascript" src="js/jquery.hoverdir.js"></script>    
        <script type="text/javascript">
            $(function() {
            
                $(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );

            });
        </script>
    
            <div class="gallery-grids">
                <ul id="da-thumbs" class="da-thumbs">
                 <li>
                    <a href="#" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/Canada.jpg" alt="">
                            <div style="display: none; left: 100%; top: 0px;">
                                <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                            </div>
                        </a>
                  </li>
                 <li>
                    <a href="#" class="b-link-stripe b-animate-go  thickbox">
                        <img src="images/Germany.png" alt="">
                        <div>
                            <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                        </div>
                    </a>
                    </li>
                    <li>
                        <a href="#" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/China.jpg" alt="">
                            <div>
                                <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/Finland.png" alt="">
                            <div>
                                <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/Hungary.jpg" alt="">
                            <div>
                                <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/Italy.png" alt="">
                            <div>
                                <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/Malta.png" alt="">
                            <div>
                                <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/Poland.jpg" alt="">
                            <div>
                                <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/Portugal.png" alt="">
                            <div>
                                <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/Sweden.jpg" alt="">
                            <div>
                                <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="b-link-stripe b-animate-go  thickbox">
                            <img src="images/USA.jpg" alt="">
                            <div>
                                <h5><span>Call </span>to</h5>
                                <p class="fa fa-phone">+8801799-382548</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
     <div class="row products_grids text-center mt-5">
            <div class="col-md-3 col-6 grid4">
                <div class="prodct1 border p-3">
                    <a href="#">
                        <img src="images/Hungary.jpg" alt="" class="img-fluid">
                        <h3 class="mt-2">Hungary</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-6 grid5">
                <div class="prodct1 border p-3">
                    <a href="#">
                        <img src="images/Portugal.png" alt="" class="img-fluid">
                        <h3 class="mt-2">Portugal</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-6 grid6 mt-md-0 mt-3">
                <div class="prodct1 border p-3">
                    <a href="#">
                        <img src="images/Poland.jpg" alt="" class="img-fluid">
                        <h3 class="mt-2">Poland</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-6 grid7 mt-md-0 mt-3">
                <div class="prodct1 border p-3">
                    <a href="#">
                        <img src="images/Sweden.jpg" alt="" class="img-fluid">
                        <h3 class="mt-2">Sweden</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row products_grids text-center mt-5">
            <div class="col-md-3 col-6 grid4">
                <div class="prodct1 border p-3">
                    <a href="#">
                        <img src="images/USA.jpg" alt="" class="img-fluid">
                        <h3 class="mt-2">USA</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-6 grid5">
                <div class="prodct1 border p-3">
                    <a href="#">
                        <img src="images/Italy.png" alt="" class="img-fluid">
                        <h3 class="mt-2">Italy</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-6 grid6 mt-md-0 mt-3">
                <div class="prodct1 border p-3">
                    <a href="#">
                        <img src="images/Malta.png" alt="" class="img-fluid">
                        <h3 class="mt-2">Malta</h3>
                        <span class="fa fa-long-arrow-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> -->
    <script type="text/javascript">
    	$('#s1').hover(function(){
		    $('.a1').toggle();
		})
		$('#s2').hover(function(){
		    $('.a2').toggle();
		})
		$('#s3').hover(function(){
		    $('.a3').toggle();
		})
		$('#s4').hover(function(){
		    $('.a4').toggle();
		})
		$('#s5').hover(function(){
		    $('.a5').toggle();
		})
		$('#s6').hover(function(){
		    $('.a6').toggle();
		})
		$('#s7').hover(function(){
		    $('.a7').toggle();
		})
		$('#s8').hover(function(){
		    $('.a8').toggle();
		})
		$('#s9').hover(function(){
		    $('.a9').toggle();
		})
		$('#s10').hover(function(){
		    $('.a10').toggle();
		})
		$('#s11').hover(function(){
		    $('.a11').toggle();
		})
    </script>
@endsection