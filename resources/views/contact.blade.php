@extends('layouts.single')

@section('content')
<div class="breadcrumb-w3pvt">
	<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="{{URL::to('/')}}">Home</a>
			</li>
			<li class="breadcrumb-item" aria-current="page">Contact</li>
		</ol>
	</nav>
	</div>
</div>
<!-- //breadcrumb -->
@if($message = Session::get('success'))
	<div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">x</button>
		<strong>{{$message}}</strong>
	</div>
@endif
<!-- contact -->
<section class="contact py-5">
	<div class="container py-lg-5">
		<h2 class="heading mb-sm-5 mb-4">Get in <strong> touch with us </strong></h2>
		<div class="row">
			<div class="col-lg-4 col-md-6 contact-address">
				<h4 class="mb-4 w3f_title">Contact Address</h4>
				<ul class="list-w3">
					<li class="d-flex"><span class="fa mt-1 mr-1 fa-map-marker"></span>House#31, Road#21, <br>Nikunju-2, Dhaka-1229</li>
					<li class="my-3"><span class="fa mr-1 fa-phone"></span>+8801799382548-9</li>
					<li class=""><span class="fa mr-1 fa-envelope"></span><a href="mailto:info@next-step.com.bd">info@next-step.com.bd</a></li>
				</ul>
				<h4 class="mt-sm-5 mt-4 mb-3 w3f_title">Follow Us</h4>
				<p>Our social links are</p>
				<ul class="list-social">
					<li><a href="#" class="facebook"><span class="fa mr-1 fa-facebook"></span></a></li>
					<li class="my-3"><a href="#" class="twitter"><span class="fa mr-1 fa-twitter"></span></a></li>
					<li class="my-3"><a href="#" class="google"><span class="fa mr-1 fa-google-plus"></span></a></li>
					<li class=""><a href="#" class="linkedin"><span class="fa mr-1 fa-linkedin"></span></a></li>
				</ul>
			</div>
			<div class="col-lg-4 col-md-6 contact-form mt-md-0 mt-sm-5 mt-4">
				<h4 class="mb-4 w3f_title">Contact Form</h4>
				<form name="contactform" id="contactform" method="post" action="{{route('send_mail')}}" onsubmit="return(validate());" novalidate="novalidate">
					{{csrf_field()}}
						<div class="form-group">
						  <label>Name</label>
						  <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name">
						</div>
						<div class="form-group">
						  <label>Email</label>
						  <input type="email" class="form-control" id="name" placeholder="Enter Email" name="email">
						</div>
						<div class="form-group">
						  <label>How can we help?</label>
						  <textarea name="message" class="form-control" id="iq" placeholder="Enter Your Message Here"></textarea>
						</div>				
						<button type="submit" class="btn btn-default">Submit</button>
					</form>
			</div>
			<div class="col-lg-4 contact-map mt-lg-0 mt-sm-5 mt-4">
				<h4 class="mb-4 w3f_title">Contact Map</h4>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14602.685123655974!2d90.3971411694256!3d23.79471723689436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c70c15ea1de1%3A0x97856381e88fb311!2sBanani%20Model%20Town%2C%20Dhaka!5e0!3m2!1sen!2sbd!4v1572347974868!5m2!1sen!2sbd" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
	</div>
</section>
@endsection